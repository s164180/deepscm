from typing import Mapping
import numpy as np
import pyro
import pyro.distributions as dist
import torch
from deepscm.arch.mnist import Decoder, Encoder
from deepscm.datasets.morphomnist import MorphoMNISTLike
from deepscm.distributions.transforms.affine import (
    ConditionalAffineTransform, LearnedAffineTransform, LowerCholeskyAffine)
from deepscm.distributions.transforms.reshape import ReshapeTransform
from pyro.distributions import (LowRankMultivariateNormal, MultivariateNormal,
                                Normal, TransformedDistribution)
from pyro.distributions.conditional import ConditionalTransformedDistribution
from pyro.distributions.torch_transform import ComposeTransformModule
from pyro.distributions.transforms import (AffineTransform, ComposeTransform,
                                           ExpTransform, SigmoidTransform,conditional_spline,
                                           Spline)
from pyro.infer import SVI, TraceGraph_ELBO
from pyro.infer.reparam.transform import TransformReparam
from pyro.nn import DenseNN, PyroModule, pyro_method
from pyro.optim import Adam
from torch.distributions import Independent

from deepscm.distributions.deep import DeepMultivariateNormal, DeepIndepNormal, Conv2dIndepNormal, DeepLowRankMultivariateNormal

import pyro.poutine as poutine


class BaseSEM(PyroModule):
    def __init__(self, preprocessing: str = 'realnvp', **kwargs):
        super().__init__()
        
        self.preprocessing = preprocessing
        
        self.register_buffer('thickness_flow_lognorm_loc', torch.zeros([], requires_grad=False))
        self.register_buffer('thickness_flow_lognorm_scale', torch.ones([], requires_grad=False))

        self.register_buffer('intensity_flow_norm_loc', torch.zeros([], requires_grad=False))
        self.register_buffer('intensity_flow_norm_scale', torch.ones([], requires_grad=False))

        self.thickness_flow_lognorm = AffineTransform(loc=self.thickness_flow_lognorm_loc.item(), scale=self.thickness_flow_lognorm_scale.item())
        self.intensity_flow_norm = AffineTransform(loc=self.intensity_flow_norm_loc.item(), scale=self.intensity_flow_norm_scale.item())

    def __setattr__(self, name, value):
        super().__setattr__(name, value)

        if name == 'thickness_flow_lognorm_loc':
            self.thickness_flow_lognorm.loc = self.thickness_flow_lognorm_loc.item()
        elif name == 'thickness_flow_lognorm_scale':
            self.thickness_flow_lognorm.scale = self.thickness_flow_lognorm_scale.item()
        elif name == 'intensity_flow_norm_loc':
            self.intensity_flow_norm.loc = self.intensity_flow_norm_loc.item()
        elif name == 'intensity_flow_norm_scale':
            self.intensity_flow_norm.scale = self.intensity_flow_norm_scale.item()
    
    def _get_preprocess_transforms(self):
        alpha = 0.05
        num_bits = 8
        
        if self.preprocessing == 'realnvp':
            # Map to [0,1]
            a1 = AffineTransform(0., (1. / 2 ** num_bits))
            
            # Map into unconstrained space as done in RealNVP
            a2 = AffineTransform(alpha, (1 - alpha))
            
            s = SigmoidTransform()
            
            preprocessing_transform = ComposeTransform([a1, a2, s.inv])
        else:
            raise ValueError
            
        return preprocessing_transform
    
    def infer_exogenous(self, **obs):        
        raise NotImplementedError
        
        

    @classmethod
    def add_arguments(cls, parser):
        parser.add_argument('--preprocessing', default='realnvp', type=str, help="type of preprocessing (default: %(default)s)", choices=['realnvp', 'glow'])

        return parser

class BaseVISEM(BaseSEM):
    context_dim = 0
    img_shape = (1, 28, 28)

    def __init__(self, hidden_dim: int, latent_dim: int, logstd_init: float = -5, decoder_type: str = 'fixed_var', decoder_cov_rank: int = 10, **kwargs):
        super().__init__(**kwargs)
        
        # Initiate hparams
        self.hidden_dim = hidden_dim
        self.latent_dim = latent_dim
        self.logstd_init = logstd_init
        
        # Prior parameters

        ## standard normal for base flow for i and t
        self.register_buffer('base_loc', torch.zeros([1, ], requires_grad=False))
        self.register_buffer('base_scale', torch.ones([1, ], requires_grad=False))
        
        ## latent x
        self.register_buffer('z_loc', torch.zeros([latent_dim, ], requires_grad=False))
        self.register_buffer('z_scale', torch.ones([latent_dim, ], requires_grad=False))
        
        ## image x
        self.register_buffer('x_base_loc', torch.zeros([1, 28, 28], requires_grad=False))
        self.register_buffer('x_base_scale', torch.ones([1, 28, 28], requires_grad=False))
        
        # decoder parts
        self.decoder_type = decoder_type
        self.decoder_cov_rank = decoder_cov_rank
        decoder = self.decoder = Decoder(self.latent_dim + self.context_dim)
        self.decoder = Conv2dIndepNormal(decoder, 1, 1)
        
        ## fixed var for decoder
        torch.nn.init.zeros_(self.decoder.logvar_head.weight)
        self.decoder.logvar_head.weight.requires_grad = False
        
        torch.nn.init.constant_(self.decoder.logvar_head.bias, self.logstd_init)
        self.decoder.logvar_head.bias.requires_grad = False
        
        # encoder of image x
        self.encoder = Encoder(self.hidden_dim)
        
        # encoder of latent space
        latent_layers = torch.nn.Sequential(torch.nn.Linear(self.hidden_dim + self.context_dim, self.hidden_dim), torch.nn.ReLU())
        self.latent_encoder = DeepIndepNormal(latent_layers, self.hidden_dim, self.latent_dim)
    
    def _get_preprocess_transforms(self):
        return super()._get_preprocess_transforms().inv
    
    def _get_transformed_x_dist(self, latent):
        x_pred_dist = self.decoder.predict(latent)
        x_base_dist = Normal(self.x_base_loc, self.x_base_scale).to_event(3)
        
        preprocess_transform = self._get_preprocess_transforms()
        
        if isinstance(x_pred_dist, MultivariateNormal) or isinstance(x_pred_dist, LowRankMultivariateNormal):
            chol_transform = LowerCholeskyAffine(x_pred_dist.loc, x_pred_dist.scale_tril)
            reshape_transform = ReshapeTransform(self.img_shape, (np.prod(self.img_shape), ))
            x_reparam_transform = ComposeTransform([reshape_transform, chol_transform, reshape_transform.inv()])
        elif isinstance(x_pred_dist, Independent):
            x_pred_dist = x_pred_dist.base_dist
            x_reparam_transform = AffineTransform(x_pred_dist.loc, x_pred_dist.scale, 3)
        
        return TransformedDistribution(x_base_dist, ComposeTransform([x_reparam_transform, preprocess_transform]))    
    
    @pyro_method
    def sample(self,n_samples=1):
        with pyro.plate('num_samples', n_samples):
            samples = self.model()
            
        return (*samples,)
    
    @pyro_method
    def reconstruct(self, x, thickness, intensity, num_particles: int=-1):
        obs = {'x': x, 'thickness': thickness, 'intensity': intensity}
        z_dist = pyro.poutine.trace(self.guide).get_trace(**obs).nodes['z']['fn']

        recons = []
        for _ in range(num_particles):
            z = pyro.sample('z', z_dist)
            
            cond_data = {'thickness': thickness, 'intensity': intensity, 'z': z}
            conditioned_model = poutine.condition(self.model, data=cond_data)

            xhat, *_ = conditioned_model()

            recons += [xhat]
        
        return torch.stack(recons).mean(0) # take mean over samples
        
    @pyro_method
    def guide(self, x, thickness, intensity):
        raise NotImplementedError()

    @pyro_method
    def svi_guide(self, x, thickness, intensity):
        self.guide(x, thickness, intensity)
    
    @pyro_method
    def svi_model(self, x, thickness, intensity):
        with pyro.plate('observations', x.shape[0]):
            self.model(x=x,thickness=thickness,intensity=intensity)
    
    @pyro_method
    def infer_exogeneous(self, **obs):
        # assuming that we use transformed distributions for everything:
        cond_sample = pyro.condition(self.sample, data=obs)
        cond_trace = pyro.poutine.trace(cond_sample).get_trace(obs['x'].shape[0])

        output = {}
        for name, node in cond_trace.nodes.items():
            if 'fn' not in node.keys():
                continue

            fn = node['fn']
            if isinstance(fn, Independent):
                fn = fn.base_dist
            if isinstance(fn, TransformedDistribution):
                output[name + '_base'] = ComposeTransform(fn.transforms).inv(node['value'])

        return output
    
    @pyro_method
    def counterfactual(self, obs: Mapping, condition: Mapping = None, num_particles: int = 1):
        _required_data = ('x', 'thickness', 'intensity')
        assert set(obs.keys()) == set(_required_data), 'got: {}'.format(tuple(obs.keys()))

        z_dist = pyro.poutine.trace(self.guide).get_trace(**obs).nodes['z']['fn']

        counterfactuals = []
        for _ in range(num_particles):
            z = pyro.sample('z', z_dist)

            exogeneous = self.infer_exogeneous(z=z, **obs)
            exogeneous = {k: v.detach() for k, v in exogeneous.items()}
            exogeneous['z'] = z
            counter = pyro.poutine.do(pyro.poutine.condition(self.sample_scm, data=exogeneous), data=condition)(obs['x'].shape[0])
            counterfactuals += [counter]
        return {k: v for k, v in zip(('x', 'z', 'thickness', 'intensity'), (torch.stack(c).mean(0) for c in zip(*counterfactuals)))}
    
    @pyro_method
    def scm(self, *args, **kwargs):
        def config(msg):
            if isinstance(msg['fn'], TransformedDistribution):
                return TransformReparam()
            else:
                return None

        return pyro.poutine.reparam(self.model, config=config)(*args, **kwargs)
    
    @pyro_method
    def sample_scm(self, n_samples=1):
        with pyro.plate('observations', n_samples):
            samples = self.scm()

        return (*samples,)
