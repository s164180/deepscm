from typing import Mapping
import numpy as np
import pyro
import pyro.distributions as dist
import torch
from deepscm.arch.mnist import Decoder, Encoder
from deepscm.datasets.morphomnist import MorphoMNISTLike
from deepscm.distributions.transforms.affine import (
    ConditionalAffineTransform, LearnedAffineTransform, LowerCholeskyAffine)
from deepscm.distributions.transforms.reshape import ReshapeTransform
from pyro.distributions import (LowRankMultivariateNormal, MultivariateNormal,
                                Normal, TransformedDistribution)
from pyro.distributions.conditional import ConditionalTransformedDistribution
from pyro.distributions.torch_transform import ComposeTransformModule
from pyro.distributions.transforms import (AffineTransform, ComposeTransform,
                                           ExpTransform, SigmoidTransform,conditional_spline,
                                           Spline)
from pyro.infer import SVI, TraceGraph_ELBO
from pyro.infer.reparam.transform import TransformReparam
from pyro.nn import DenseNN, PyroModule, pyro_method
from pyro.optim import Adam
from torch.distributions import Independent

from .pyro_model import BaseVISEM


class FullVISEM(BaseVISEM):
    context_dim = 2

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        # Flow for modelling t Gamma
        self.thickness_flow_components = conditional_spline(1, 1, count_bins=8, bound=3., order='linear') # default parameters
        self.thickness_flow_transforms = ComposeTransform([self.thickness_flow_lognorm, ExpTransform()])
        self.thickness_flow = [self.thickness_flow_components, self.thickness_flow_transforms]

        # affine flow for s normal
        self.intensity_flow_components = ComposeTransformModule([LearnedAffineTransform(), Spline(1, count_bins=8)])
        self.intensity_flow_transforms = ComposeTransform([SigmoidTransform(), self.intensity_flow_norm])
        self.intensity_flow = ComposeTransform([self.intensity_flow_components, self.intensity_flow_transforms])
    
    @pyro_method
    def pgm_model(self, x=None, thickness=None, intensity=None):
        # sample intensity
        intensity_base_dist = Normal(self.base_loc, self.base_scale).to_event(1)
        intensity_dist = TransformedDistribution(intensity_base_dist, self.intensity_flow)
        intensity = pyro.sample('intensity', intensity_dist, obs=intensity)
        intensity_ = self.intensity_flow_norm.inv(intensity)
        # pseudo call to intensity_flow_transforms to register with pyro
        _ = self.intensity_flow_components

        # sample thickness
        thickness_base_dist = Normal(self.base_loc, self.base_scale).to_event(1)
        thickness_dist = ConditionalTransformedDistribution(thickness_base_dist, self.thickness_flow).condition(intensity_)
        thickness = pyro.sample('thickness', thickness_dist, obs=thickness)
        _ = self.thickness_flow_components

        return thickness, intensity

    
    @pyro_method
    def model(self, x=None, thickness=None, intensity=None):
        # sample intensity
        intensity_base_dist = Normal(self.base_loc, self.base_scale)
        intensity_dist = TransformedDistribution(intensity_base_dist, self.intensity_flow).to_event(1)
        intensity = pyro.sample('intensity', intensity_dist, obs=intensity)
        # pseudo call to intensity_flow_transforms to register with pyro
        _ = self.intensity_flow_components

        # sample thickness
        intensity_ = self.intensity_flow_norm.inv(intensity)
        thickness_base_dist = Normal(self.base_loc, self.base_scale).to_event(1)
        thickness_dist = ConditionalTransformedDistribution(thickness_base_dist, self.thickness_flow).condition(intensity_)
        thickness = pyro.sample('thickness', thickness_dist, obs=thickness)
        _ = self.thickness_flow_components

        # latent
        z_dist = Normal(self.z_loc, self.z_scale).to_event(1)
        z = pyro.sample('z', z_dist)

        # x
        thickness_ = self.thickness_flow_transforms.inv(thickness)
        latent = torch.cat([z, thickness_, intensity_], 1) 
        x_dist = self._get_transformed_x_dist(latent)

        x = pyro.sample('x', x_dist, obs=x)

        return x, z, thickness, intensity

    @pyro_method
    def guide(self, x, thickness, intensity):
        with pyro.plate('observations', x.shape[0]):
            hidden = self.encoder(x)

            thickness_ = self.thickness_flow_transforms.inv(thickness)
            intensity_ = self.intensity_flow_norm.inv(intensity)

            hidden = torch.cat([hidden, thickness_, intensity_], 1)

            latent_dist = self.latent_encoder.predict(hidden)

            z = pyro.sample('z', latent_dist)

        return z