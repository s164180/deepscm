
import torch
import pytorch_lightning as pl
import matplotlib.pyplot as plt
import numpy as np


from argparse import ArgumentParser
#from deepscm.load_model import load_model
import torch
import os

from pytorch_lightning import Trainer
from pytorch_lightning.loggers import TensorBoardLogger
from pytorch_lightning.callbacks.early_stopping import EarlyStopping

from argparse import Namespace
import torchvision.utils


if __name__ == '__main__':
    
    # Add program specific arguments
    parser = ArgumentParser()

    parser.add_argument("--ckpt_path", type=str, required=True, help="model checkpoint path)")
    parser.add_argument('--test_batch_size', type=int, default = 4, help="Test batch size")
    temp_args, _ = parser.parse_known_args()
    
    # split ckpt path
    path = os.path.normpath(temp_args.ckpt_path)
    split_path = path.split(os.sep)

    #take these from this
    temp_args.default_root_dir = os.path.join(split_path[0],split_path[1],split_path[2])
    temp_args.model_name = split_path[0]
    temp_args.name = split_path[1]
    temp_args.job_num = split_path[2]

    # Pull the model name and class
    LitModelClass = load_model(temp_args)
    LitModel = LitModelClass.load_from_checkpoint(checkpoint_path=temp_args.ckpt_path, test_batch_size = temp_args.test_batch_size)

    # trainer args
    train_parser = ArgumentParser()
    train_parser = Trainer.add_argparse_args(train_parser)
    train_args, _ = train_parser.parse_known_args()

    if torch.cuda.is_available():
        trainer = Trainer.from_argparse_args(train_args, accelerator='gpu', devices=1)
    else:
        trainer = Trainer.from_argparse_args(train_args)
    
    logger = TensorBoardLogger(temp_args.model_name, temp_args.name, version=temp_args.job_num)
    trainer.logger = logger
    trainer.test(LitModel, ckpt_path = temp_args.ckpt_path)







