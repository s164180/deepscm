# %% Load modules and functions

import numpy as np
import os
import pandas as pd
import pyro
import torch

from pyro.distributions import Gamma, Normal, TransformedDistribution, Categorical, Uniform
from pyro.distributions.transforms import SigmoidTransform, AffineTransform, ComposeTransform
from tqdm import tqdm

from deepscm.datasets.morphomnist import load_morphomnist_like, save_morphomnist_like
from deepscm.datasets.morphomnist.transforms import SetThickness, ImageMorphology
from pyro.distributions.torch_transform import ComposeTransformModule

from multiprocessing import Pool

def pool_fun(enum):
    n, (thickness, intensity) = enum
    morph = ImageMorphology(images_[n], scale=16)
    tmp_img = morph.downscale(np.float32(SetThickness(thickness)(morph)))
    avg_intensity = get_intensity(tmp_img)
    
    mult = intensity.numpy() / avg_intensity
    tmp_img = np.clip(tmp_img * mult, 0, 255)

    #images[n] = tmp_img

    return n, tmp_img


def get_intensity(img):
    threshold = 0.5

    img_min, img_max = img.min(), img.max()
    mask = (img >= img_min + (img_max - img_min) * threshold)
    avg_intensity = np.median(img[mask])

    return avg_intensity

def model_their(n_samples=None, scale=0.5, invert=False):
    with pyro.plate('observations', n_samples):
        thickness = 0.5 + pyro.sample('thickness', Gamma(10., 5.))

        if invert:
            loc = (thickness - 2) * -2
        else:
            loc = (thickness - 2.5) * 2

        transforms = ComposeTransform([SigmoidTransform(), AffineTransform(64, 191)])

        intensity = pyro.sample('intensity', TransformedDistribution(Normal(loc, scale), transforms))

    return thickness, intensity

def model_o(n_samples=None, scale=0.5, invert=False):
    with pyro.plate('observations', n_samples):
        eps_i1 = pyro.sample("eps_i1", Gamma(10,5)) + 0.5
        eps_i2 = pyro.sample("eps_i2", Normal(0,1))
        
        intensity = 190 * SigmoidTransform()(0.5 * eps_i2 + 2 * eps_i1 - 5) + 64
        
        eps_t = pyro.sample("eps_t", Normal(0,1))
        thickness = torch.max(-torch.log(-(intensity-255)/(intensity-64)) + 4 - 1/2 * eps_t,torch.tensor([0.1]))
        
    return thickness, intensity

    
def gen_dataset(args, train=True, multiprocessing = True):
    pyro.clear_param_store()
    global images_
    images_, labels, _ = load_morphomnist_like(args.data_dir, train=train)
    #images_ = images_[:50]

    images = np.zeros_like(images_)

    if args.digit_class is not None:
        mask = (labels == args.digit_class)
        images_ = images_[mask]
        labels = labels[mask]
    
    n_samples = len(images_)
    with torch.no_grad():
        thickness, intensity = model(n_samples, scale=args.scale, invert=args.invert)

    metrics = pd.DataFrame(data={'thickness': thickness, 'intensity': intensity})
    
    # # Use multiprocesing instead
    
    if multiprocessing:
        with Pool(32) as p:
            
            r = list(tqdm(p.imap(pool_fun, enumerate(zip(thickness, intensity))), total=n_samples))

        for (n, im) in r:
            images[n] = im

    else:
        for n, (thickness, intensity) in enumerate(tqdm(zip(thickness, intensity), total=n_samples)):
            morph = ImageMorphology(images_[n], scale=16)
            tmp_img = morph.downscale(np.float32(SetThickness(thickness)(morph)))

            avg_intensity = get_intensity(tmp_img)

            mult = intensity.numpy() / avg_intensity
            tmp_img = np.clip(tmp_img * mult, 0, 255)

            images[n] = tmp_img

    save_morphomnist_like(images, labels, metrics, args.out_dir, train=train)


# %% Main code

if __name__ == '__main__':
    import argparse

    # args = parser.parse_args()
    
    from argparse import Namespace
    
    args = Namespace()
    args.out_dir = './generated_data'
    args.data_dir = 'assets/data/all_morphomnist/original'
    args.digit_class = None
    args.scale = 0.5
    args.invert = False

    print(f'Generating data for:\n {args.__dict__}')

    os.makedirs(args.out_dir, exist_ok=True)
    with open(os.path.join(args.out_dir, 'args.txt'), 'w') as f:
        print(f'Generated data for:\n {args.__dict__}', file=f)

    print('Generating Training Set')
    print('#######################')
    gen_dataset(args, True)

    print('Generating Test Set')
    print('###################')
    gen_dataset(args, False)

# %%
