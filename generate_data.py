

# %% Load packages

import numpy as np
import os
import pandas as pd
import pyro
import torch

import matplotlib.pyplot as plt

from pyro.distributions import Gamma, Normal, TransformedDistribution
from pyro.distributions.transforms import SigmoidTransform, AffineTransform, ComposeTransform, ExpTransform

from tqdm import tqdm

# %% 

def model(n_samples=None):
    with pyro.plate('observations', n_samples):
        
        eps_i1 = pyro.sample("eps_i1", Gamma(10,5)) + 0.5
        eps_i2 = pyro.sample("eps_i2", Normal(0,1))
        
        intensity = 190 * SigmoidTransform()(0.5 * eps_i2 + 2 * eps_i1 - 5) + 64
        
        eps_t = pyro.sample("eps_t", Normal(0,1))
        
        thickness =  torch.max(-torch.log(-(intensity-255)/(intensity-64)) + 4 - 1/2 * eps_t,torch.tensor([0.3]))
    
    return intensity, thickness

# %%

n_samples = 10000
intensity, thickness = model(n_samples)

# %%

import seaborn as sns
sns.set_theme()
h = sns.jointplot(x=thickness, y=intensity, marginal_ticks=True, kind = 'hist', joint_kws ={'bins':50, "cbar": True, "stat": "count"})
h.set_axis_labels('Thickness', 'Intensity', fontsize=16)


# %%
