
# %% Load packages
import torch
import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
sns.set_theme()

from deepscm.morphomnist import measure
import multiprocessing

# %% Paths
#ckpt_path = "morphomnist/generated_data_independent/version_13/checkpoints/epoch=249-step=52750.ckpt"
#ckpt_path = "morphomnist/generated_data_conditional/version_5/checkpoints/epoch=249-step=52750.ckpt"
#ckpt_path = "morphomnist/generated_data_reverse/version_8/checkpoints/epoch=249-step=52750.ckpt"
#ckpt_path = "morphomnist/generated_data_full/version_0/checkpoints/epoch=249-step=52750.ckpt"


def measure_image(x, normalize=False, threshold=0.5):
    # imgs = x.detach().cpu().numpy()[:, 0]
    imgs = x.detach().cpu().numpy()

    if normalize:
        imgs -= imgs.min()
        imgs /= imgs.max() + 1e-6

    with multiprocessing.Pool() as pool:
        measurements = measure.measure_batch(imgs, threshold=threshold, pool=pool)

    def get_intensity(imgs, threshold):

        img_min, img_max = imgs.min(axis=(1, 2), keepdims=True), imgs.max(axis=(1, 2), keepdims=True)
        mask = (imgs >= img_min + (img_max - img_min) * threshold)

        return np.array([np.median(i[m]) for i, m in zip(imgs, mask)])

    return measurements['thickness'].values, get_intensity(imgs, threshold)


# %% titles and sources

source = ['True',
        "morphomnist/generated_data_independent/version_13/",
        "morphomnist/generated_data_conditional/version_5/",
        "morphomnist/generated_data_reverse/version_8/",
        "morphomnist/generated_data_full/version_0/"]
titles = ['Model', 'Independent','Conditional', 'Reverse', 'Full']



# %% Code for model
from deepscm.datasets.morphomnist import MorphoMNISTLike
mnist_test = MorphoMNISTLike('./generated_data', train=False, columns=['thickness', 'intensity'])

thickness_test = mnist_test.metrics['thickness']
intensity_test = mnist_test.metrics['intensity']

try:
    measured_intensity is None
except:
    measured_thickness, measured_intensity = measure_image(mnist_test.images)

# %% Log probabilities

paths = [f'{x}metrics.pt' for x in source[1:]]
titles = ['Ind.','Cond.', 'Rev.', 'Full']

metrics_df = pd.DataFrame(columns=['Experiment', 'loss', 'log p(x)', 'log p(intensity)', 'log p(thickness)', 'log p(z) - log q(z)', 'p(z)', 'q(z)'])

for i, (path,title) in enumerate(zip(paths, titles)):
    metrics = torch.load(path)

    for k, v in metrics.items():
        print(f'{k}: {v:.0f}')
    

    row = {k.replace('test/', ''): torch.tensor([v]).item() for k, v in metrics.items()}
    metrics_df.loc[len(metrics_df)] = {'Experiment': title, **row}


# Add reconstruction losses
paths = [f'{x}/reconstructions.pt' for x in source[1:]]
recon_mae = {}
for i, (path,title) in enumerate(zip(paths, titles)):
    x = torch.load(path)['ims']
    x_o = x[:8]
    x_r = x[8:]
    

    recon_mae[title] = (x_r - x_o).abs().mean().item()

metrics_df.loc[:, 'recon_mae'] = list(recon_mae.values())
metrics_df

# %% Density Histograms
paths = [f'{x}covariate_samples.pt' for x in source]
titles = ['test', 'independent','conditional', 'reverse', 'full']

a = torch.load(paths[1])
test_data = pd.read_csv('generated_data/t10k-morpho.csv',index_col=0)
sns.set_theme()
# h = sns.jointplot(data=test, x="thickness", y="intensity", marginal_ticks=True, kind = 'hist', joint_kws ={'bins':50, "cbar": True, "stat": "count"},  xlim=(0,10))
# h.set_axis_labels('Thickness', 'Intensity', fontsize=16)

for i, (path,title) in enumerate(zip(paths, titles)):
    if title == 'test':
        df = test_data
    else:
        df = torch.load(path)[['thickness', 'intensity']]
    
    h = sns.jointplot(data=df, x="thickness", y="intensity", marginal_ticks=True, kind = 'hist', joint_kws ={'bins':50, "cbar": True, "stat": "count"},  xlim=(0,10))
    h.set_axis_labels('Thickness', 'Intensity', fontsize=16)
    h.savefig(f'thesis_plots/{title}_hist.pdf')

# %% covariate figures

path = [f'{x}/covariate_samples.pt' for x in source]
titles = ['Model', 'Ind.','Cond.', 'Rev.', 'Full']

fig, axs = plt.subplots(2,5, figsize =(20,7), sharey='row') #, sharex='col')

for i, (path,title) in enumerate(zip(path, titles)):
    if title != 'Model':
        df = torch.load(path)
    else:
        df = pd.DataFrame({'thickness': thickness_test, 'intensity': intensity_test, 'measured_thickness': measured_thickness, 'measured_intensity': measured_intensity})
    
    ax = axs[0,i]
    sns.scatterplot(data=df, x="thickness", y="measured_thickness",alpha=0.05,ax=ax)
    ax.set_title(title)
    ax.set_xlabel('Sampled Thickness')
    ax.set_ylabel('Measured Thicknedss')
    ax.set_xlim(0.5, 8)
    ax.set_ylim(0.5, 8)
    

    ax = axs[1,i]
    sns.scatterplot(data=df, x="intensity", y="measured_intensity",alpha=0.05,ax=ax)
    #ax.set_title(title)
    ax.set_xlabel('Sampled Intensity')
    ax.set_ylabel('Measured Intensity')
    ax.set_xlim(64, 255)
    ax.set_ylim(64, 255)
fig.savefig('./thesis_plots/covariates.pdf', bbox_inches='tight',dpi=10)

# %% Reconstructions

import matplotlib as mpl
mpl.rcParams['figure.dpi'] = 300

from matplotlib import cm
cmaps = [cm.Reds, cm.Blues, cm.Greens]
img_cm = 'Greys_r'

paths = [f'{x}/reconstructions.pt' for x in source]
titles = ['Original', 'Ind.','Cond.', 'Rev.', 'Full']

num_samples = 8

fig = plt.figure(figsize=(num_samples * 1., 5 * 1.))
gs = fig.add_gridspec(5, num_samples, wspace=0., hspace=0.0)

for i, (path, title) in enumerate(zip(paths, titles)):

    if title == "Original":
        ims = torch.load(paths[1])['ims'][:8]
    else:
        ims = torch.load(path)['ims'][8:]
    
    for j in range(num_samples):
        img = ims[j].cpu()
        ax = fig.add_subplot(gs[i, j])
        ax.imshow(img.numpy().squeeze(), img_cm, vmin=0, vmax=255)

        if j == 0:
            ax.set_ylabel(title)
            for s in ['top', 'right']:
                ax.spines[s].set_visible(False)
            for s in ['left', 'top', 'right']:
                ax.spines[s].set_visible(False)
            for s in ['top', 'right', 'bottom']:
                ax.spines[s].set_visible(False)
            ax.yaxis.grid(False)
            ax.xaxis.grid(False)
            plt.setp(ax.yaxis.get_majorticklines(), visible=False)
            plt.setp(ax.yaxis.get_minorticklines(), visible=False)
            plt.setp(ax.xaxis.get_majorticklines(), visible=False)
            plt.setp(ax.xaxis.get_minorticklines(), visible=False)
            plt.setp(ax.get_yticklabels(), visible=False)
            plt.setp(ax.get_xticklabels(), visible=False)
        else:
            ax.axis('off')
        
        ax.xaxis.set_major_locator(plt.NullLocator())
        ax.yaxis.set_major_locator(plt.NullLocator())

plt.gca().xaxis.set_major_locator(plt.NullLocator())
plt.gca().yaxis.set_major_locator(plt.NullLocator())

plt.savefig(f"thesis_plots/reconstructions.pdf", bbox_inches='tight', pad_inches=0)
plt.show()


# %% Image sample
paths = [f'{x}image_samples.pt' for x in source]
titles = ['Original', 'Ind.','Cond.', 'Rev.', 'Full']

num_samples = 8

fig = plt.figure(figsize=(num_samples * 1., 4 * 1.))
gs = fig.add_gridspec(4, num_samples, wspace=0., hspace=0.0)

for i, (path, title) in enumerate(zip(paths, titles)):

    if title == "Original":
        pass
    else:
        ims = torch.load(path)[:8]
        for j in range(num_samples):
            img = ims[j].cpu()
            ax = fig.add_subplot(gs[i-1, j])
            ax.imshow(img.numpy().squeeze(), img_cm, vmin=0, vmax=255)

            if j == 0:
                ax.set_ylabel(title)
                for s in ['top', 'right']:
                    ax.spines[s].set_visible(False)
                for s in ['left', 'top', 'right']:
                    ax.spines[s].set_visible(False)
                for s in ['top', 'right', 'bottom']:
                    ax.spines[s].set_visible(False)
                ax.yaxis.grid(False)
                ax.xaxis.grid(False)
                plt.setp(ax.yaxis.get_majorticklines(), visible=False)
                plt.setp(ax.yaxis.get_minorticklines(), visible=False)
                plt.setp(ax.xaxis.get_majorticklines(), visible=False)
                plt.setp(ax.xaxis.get_minorticklines(), visible=False)
                plt.setp(ax.get_yticklabels(), visible=False)
                plt.setp(ax.get_xticklabels(), visible=False)
            else:
                ax.axis('off')
            
            ax.xaxis.set_major_locator(plt.NullLocator())
            ax.yaxis.set_major_locator(plt.NullLocator())

plt.gca().xaxis.set_major_locator(plt.NullLocator())
plt.gca().yaxis.set_major_locator(plt.NullLocator())
plt.savefig(f"thesis_plots/samples.pdf", bbox_inches='tight', pad_inches=0)
plt.show()

# %% Conditional samples
paths = [f'{x}cond_sampels.pt' for x in source]
titles = ['Original', 'Ind.','Cond.', 'Rev.', 'Full']

thickness_range = [1, 2, 4, 6]
intensity_range = [64, 128, 192, 255]

for i, (path, title) in enumerate(zip(paths, titles)):
    if title == 'Original':
        continue
    cond_im = torch.load(path)

    num_cols = len(thickness_range)
    num_rows = len(intensity_range)
    num = num_cols * num_rows

    fig = plt.figure(figsize=(num_cols * 1.1, num_rows * 1.1))
    gs = fig.add_gridspec(num_rows, num_cols, wspace=.2, hspace=.2)

    for i in range(num):
        x = cond_im[i].cpu()
        
        ax = fig.add_subplot(gs[i // num_cols, i % num_cols])
        ax.imshow(x.numpy().squeeze(), img_cm, vmin=0, vmax=255)
        if (i % num_cols) == 0:
            thickness = thickness_range[i // num_cols]
            ax.set_ylabel(f'$t={thickness:.1f}$', fontsize=mpl.rcParams['axes.titlesize'])
            for s in ['top', 'right']:
                ax.spines[s].set_visible(False)
            for s in ['left', 'top', 'right']:
                ax.spines[s].set_visible(False)
            for s in ['top', 'right', 'bottom']:
                ax.spines[s].set_visible(False)
            ax.yaxis.grid(False)
            ax.xaxis.grid(False)
            plt.setp(ax.yaxis.get_majorticklines(), visible=False)
            plt.setp(ax.yaxis.get_minorticklines(), visible=False)
            plt.setp(ax.xaxis.get_majorticklines(), visible=False)
            plt.setp(ax.xaxis.get_minorticklines(), visible=False)
            plt.setp(ax.get_yticklabels(), visible=False)
            plt.setp(ax.get_xticklabels(), visible=False)
        else:
            ax.axis('off')
        if i // num_cols == 0:
            intensity = intensity_range[i]
            ax.set_title(f'$i={intensity:.0f}$')
        ax.xaxis.set_major_locator(plt.NullLocator())
        ax.yaxis.set_major_locator(plt.NullLocator())

    plt.gca().xaxis.set_major_locator(plt.NullLocator())
    plt.gca().yaxis.set_major_locator(plt.NullLocator())
    
    plt.savefig(f"thesis_plots/cond_samples_{title}.pdf", bbox_inches='tight', pad_inches=0)
    
    plt.show()

# %% Intervention KDE
paths = [f'{x}prob_maps.pt' for x in source[2:]]
titles = ['Cond.', 'Rev.', 'Full']

import matplotlib as mpl
mpl.rcParams['figure.dpi'] = 300

from matplotlib import cm
cmaps = [cm.Reds, cm.Blues, cm.Greens]
img_cm = 'Greys_r'


prob_maps = {}

for path, title in zip(paths, titles):
    prob_maps[title] = torch.load(path)


intensity_range = torch.arange(64, 255, 0.5, dtype=torch.float)
thickness_range = torch.arange(.5, 9, 0.01, dtype=torch.float)


num_intensity = intensity_range.shape[0]
num_thickness = thickness_range.shape[0]

intensity_range = intensity_range.repeat(num_thickness).unsqueeze(1)
thickness_range = thickness_range.repeat_interleave(num_intensity).unsqueeze(1)


from matplotlib.colors import ListedColormap

def make_transparent_cmap(cmap):
    n = 256
    colours = cmap(np.linspace(0, 1, n))
    colours[:, -1] = np.linspace(0, 1, n)
    return ListedColormap(colours) 


# %% Add model distributions
from deepscm.datasets.morphomnist.create_synth_intensity_thickness_data import model_their as model
import pyro
prob_maps['True'] = {}

cond_data = {
            'thickness': thickness_range[:, 0] -.5,
            'intensity': intensity_range[:, 0]
        }
with torch.no_grad():
    trace = pyro.poutine.trace(pyro.condition(model, data=cond_data)).get_trace(num_intensity*num_thickness)
    trace.compute_log_prob()
log_prob = trace.nodes['thickness']['log_prob'] + trace.nodes['intensity']['log_prob']
prob_maps['True']['$p(t, i)$'] = {
        'log_prob': log_prob, 'intensity': trace.nodes['_RETURN']['value'][1], 'thickness': trace.nodes['_RETURN']['value'][0]
    }

intervention_data = {'thickness': cond_data['thickness'] + 1}
with torch.no_grad():
    trace = pyro.poutine.trace(pyro.condition(pyro.do(model, data=intervention_data), data=cond_data)).get_trace(num_intensity*num_thickness)
    trace.compute_log_prob()

log_prob = trace.nodes['thickness']['log_prob'] + trace.nodes['intensity']['log_prob']

prob_maps['True']['$p(t, i\,|\,do(t + 1))$'] = {
        'log_prob': log_prob, 'intensity': trace.nodes['_RETURN']['value'][1], 'thickness': trace.nodes['_RETURN']['value'][0]
    }

intervention_data = {'thickness': np.clip(cond_data['thickness'] - 0.5, 0., 10)}
with torch.no_grad():
    trace = pyro.poutine.trace(pyro.condition(pyro.do(model, data=intervention_data), data=cond_data)).get_trace(num_intensity*num_thickness)
    trace.compute_log_prob()

log_prob = trace.nodes['thickness']['log_prob'] + trace.nodes['intensity']['log_prob']
prob_maps['True']['$p(t, i\,|\,do(t - 0.5))$'] = {
    'log_prob': log_prob, 'intensity': trace.nodes['_RETURN']['value'][1], 'thickness': trace.nodes['_RETURN']['value'][0]
}

titles.append('True')



# %%
import seaborn as sns

n_exp = 3
fig = plt.figure(figsize=(15, 5))
ratio = 5
global_gs = fig.add_gridspec(1, 3)

ax_joint0 = ax_marg_x0 = ax_marg_y0 = None


g = sns.jointplot(data=d, x="thickness", y="intensity", kind="hist", alpha=0.3)


# %%

def plot_exp(i, exp, ax_joint, ax_marg_x, ax_marg_y):
    contours = []
    for j, (k, v) in enumerate(prob_maps[exp].items()):
        thickness = v['thickness'].reshape(num_thickness, num_intensity).numpy()
        intensity = v['intensity'].reshape(num_thickness, num_intensity).numpy()
        log_prob = v['log_prob'].reshape(num_thickness, num_intensity).numpy()
        prob = np.exp(log_prob)


        valid = thickness[:, 0] > 0.5
        thickness = thickness[valid, :]
        intensity = intensity[valid, :]
        log_prob = log_prob[valid, :]
        prob = prob[valid, :]
        
        dt = thickness[1, 0] - thickness[0, 0]
        di = intensity[0, 1] - intensity[0, 0]
        marg_t = prob.sum(1) * dt
        marg_i = prob.sum(0) * di
        
        mi = prob.min()
        ma = np.percentile(prob.flat, 99.5)
        step = (ma - mi) / 5
        levels = np.arange(mi, ma, step) + step
        
        filled = j == 0
        cmap = cmaps[j]
        colour = cmap(.7)
        
        alpha = .7
        if filled:
            ax_joint.contourf(thickness, intensity, prob, cmap=cmap, antialiased=True, levels=levels, alpha=alpha, extend='max')
            c = ax_marg_x.fill_between(thickness[:, 0], marg_t, color=colour, lw=0, alpha=alpha)
            ax_marg_y.fill_betweenx(intensity[0, :], marg_i, color=colour, lw=0, alpha=alpha)
        else:
            kwargs = dict(cmap=cmap.reversed(), antialiased=True, levels=levels, alpha=alpha)
            if j == 2:
                kwargs['linestyles'] = 'dashed'
            c = ax_joint.contour(thickness, intensity, prob, **kwargs)
        
        kwargs = dict(ls='--') if j == 2 else {}
        h, = ax_marg_x.plot(thickness[:, 0], marg_t, c=colour, **kwargs)
        ax_marg_y.plot(marg_i, intensity[0, :], c=colour, **kwargs)
        
        if filled:
            contours.append(c)
        else:
            contours.append(h)            

    ax_marg_x.set_title(exp)
    ax_joint.set_xlabel('thickness ($t$)', fontsize='medium')
    if i == 0:
        ax_joint.set_ylabel('intensity ($i$)', fontsize='medium')
        ax_joint.legend(contours, prob_maps[exp].keys(), loc='lower right', fontsize='small')

titles = ['Cond.', 'Rev.', 'Full']
def plot_all_joints():
    n_exp = len(titles)
    fig = plt.figure(figsize=(15, 5))
    ratio = 5
    global_gs = fig.add_gridspec(1, 3)
    
    ax_joint0 = ax_marg_x0 = ax_marg_y0 = None

    margin = 0.02 / n_exp
    width = 1. / n_exp
    for i, exp in enumerate(titles):
        gs = global_gs[0,i].subgridspec(2, 2, width_ratios=[ratio, 1], height_ratios=[1, ratio],
                                wspace=0.1, hspace=0.1)

        ax_joint = fig.add_subplot(gs[1, 0], sharex=ax_joint0, sharey=ax_joint0)
        ax_marg_x = fig.add_subplot(gs[0, 0], sharex=ax_joint0, sharey=ax_marg_x0)
        ax_marg_y = fig.add_subplot(gs[1, 1], sharey=ax_joint0, sharex=ax_marg_y0)
        # if i % 2 == 0:
        #     ax_joint0 = ax_joint
        #     ax_marg_x0 = ax_marg_x
        #     ax_marg_y0 = ax_marg_y
        
        plt.setp(ax_marg_x.get_xticklabels(), visible=False)
        plt.setp(ax_marg_y.get_yticklabels(), visible=False)
        
        # Turn off the ticks on the density axis for the marginal plots
        plt.setp(ax_marg_x.yaxis.get_majorticklines(), visible=False)
        plt.setp(ax_marg_x.yaxis.get_minorticklines(), visible=False)
        plt.setp(ax_marg_y.xaxis.get_majorticklines(), visible=False)
        plt.setp(ax_marg_y.xaxis.get_minorticklines(), visible=False)
        plt.setp(ax_marg_x.get_yticklabels(), visible=False)
        plt.setp(ax_marg_y.get_xticklabels(), visible=False)
        for s in ['top', 'right']:
            ax_joint.spines[s].set_visible(False)
        for s in ['left', 'top', 'right']:
            ax_marg_x.spines[s].set_visible(False)
        for s in ['top', 'right', 'bottom']:
            ax_marg_y.spines[s].set_visible(False)
        ax_marg_x.yaxis.grid(False)
        ax_marg_y.xaxis.grid(False)

        if exp == 'True':
            #ax_joint.hist2d(t_s.cpu().numpy(),i_s.cpu().numpy(), bins=100, density=True, cmin=0.001)
            #sns.kdeplot()
            pass
        else:
            # if i % 2 == 0:
            #     plt.setp(ax_joint.get_yticklabels(), visible=False)

            plot_exp(i, exp, ax_joint, ax_marg_x, ax_marg_y)
    
    fig.tight_layout()
    fig.savefig(f'./thesis_plots/intervention_kde.pdf', bbox_inches='tight',dpi=100)
plot_all_joints()
plt.show()

# %% Intervention vs Condition (reverse model)
from deepscm.util import load_model
from argparse import ArgumentParser
from argparse import Namespace
import pyro
import torch

# pick reverse
path = [f'{x}int_cond_dict.pt' for x in source[1:]][-2] 

int_cond_dict = torch.load(path)

intensity_range = torch.arange(64, 255, 0.5, dtype=torch.float)
thickness_range = torch.arange(.5, 9, 0.01, dtype=torch.float)
num_intensity = intensity_range.shape[0]
num_thickness = thickness_range.shape[0]

intensity_range = intensity_range.repeat(num_thickness).unsqueeze(1)
thickness_range = thickness_range.repeat_interleave(num_intensity).unsqueeze(1)

thickness = thickness_range.reshape(num_thickness, num_intensity)
intensity = intensity_range.reshape(num_thickness, num_intensity)

prob = prob_maps['Rev.']['$p(t, i)$']['log_prob'].exp().reshape(num_thickness, num_intensity)

# % ploting 

def my_violinplot(var, val, loc, ax=None, vert=False, width=None, fill_kw=None, line_kw=None):
    if ax is None:
        ax = plt.gca()
    if width is not None:
        val = .5 * width * val / max(val)
    fill_kw_ = dict()  # defaults
    fill_kw_.update(fill_kw or {})
    line_kw_ = dict()  # defaults
    line_kw_.update(line_kw or {})
    if vert:
        fh = ax.fill_betweenx(var, loc - val, loc + val, **fill_kw_)
        lh, = ax.plot([loc, loc], [min(var), max(var)], **line_kw_)
    else:
        fh = ax.fill_between(var, loc - val, loc + val, **fill_kw_)
        lh, = ax.plot([min(var), max(var)], [loc, loc], **line_kw_)
    return fh, lh


fig, axs = plt.subplots(1, 3, figsize=(9, 3), sharex=True, sharey=True, gridspec_kw=dict(wspace=.1))
mi = prob.min()
ma = np.percentile(prob.view(-1), 99.5)
step = (ma - mi) / 5
levels = torch.arange(mi, ma, step) + step

for ax in axs:
    ax.contour(thickness, intensity, prob, cmap=cmaps[0].reversed(), levels=levels, alpha=.5, antialiased=True, zorder=-1)

interv_i = [96, 160, 224]
interv_t = [1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5]

# first condition on i
for do_i in interv_i:
    cond_i = int_cond_dict['cond_i'][do_i]
    thickness = cond_i['thickness']
    prob = cond_i['prob']
    color = cmaps[1](.75)
    
    my_violinplot(thickness[:, 0], prob, do_i, vert=False, ax=axs[0], width=.75*50,
                  fill_kw=dict(alpha=.7, facecolor=color), line_kw=dict(color=color, ls=':'))

# intervention on i
for do_i in interv_i:
    cond_i = int_cond_dict['do_i'][do_i]
    thickness = cond_i['thickness']
    prob = cond_i['prob']
    color = cmaps[1](.75)
    my_violinplot(thickness[:, 0], prob, do_i, vert=False, ax=axs[1], width=.75*50,#25,
                  fill_kw=dict(alpha=.7, facecolor=color), line_kw=dict(color=color, ls=':'))

# condition on t
for do_t in interv_t:
    cond_t = int_cond_dict['cond_t'][do_t]
    intensity = cond_t['intensity']
    prob = cond_t['prob']
    color = cmaps[2](.75)
    my_violinplot(intensity[0, :], prob, do_t, vert=True, ax=axs[2], width=.75*1.,
                  fill_kw=dict(alpha=.7, facecolor=color), line_kw=dict(color=color, ls=':'))

axs[0].set_title("$p(t\,|\,i)$")
axs[1].set_title("$p(t\,|\,do(i)) = p(t)$")
axs[2].set_title("$p(i\,|\,do(t)) = p(i\,|\,t)$")
for ax in axs:
    ax.set_xlabel('Thickness ($t$)', fontsize='medium')
axs[0].set_ylabel('Intensity ($i$)', fontsize='medium');
fig.savefig(f'./thesis_plots/int_cond_reverse.pdf', bbox_inches='tight',dpi=100)
plt.show()

# %% Intervention vs Condition (FULL model)
from deepscm.util import load_model
from argparse import ArgumentParser
from argparse import Namespace
import pyro
import torch

# pick full
path = [f'{x}int_cond_dict.pt' for x in source[1:]][-1] 

int_cond_dict = torch.load(path)

intensity_range = torch.arange(64, 255, 0.5, dtype=torch.float)
thickness_range = torch.arange(.5, 9, 0.01, dtype=torch.float)
num_intensity = intensity_range.shape[0]
num_thickness = thickness_range.shape[0]

intensity_range = intensity_range.repeat(num_thickness).unsqueeze(1)
thickness_range = thickness_range.repeat_interleave(num_intensity).unsqueeze(1)

thickness = thickness_range.reshape(num_thickness, num_intensity)
intensity = intensity_range.reshape(num_thickness, num_intensity)

prob = prob_maps['Full']['$p(t, i)$']['log_prob'].exp().reshape(num_thickness, num_intensity)

fig, axs = plt.subplots(1, 3, figsize=(9, 3), sharex=True, sharey=True, gridspec_kw=dict(wspace=.1))
mi = prob.min()
ma = np.percentile(prob.view(-1), 99.5)
step = (ma - mi) / 5
levels = torch.arange(mi, ma, step) + step

for ax in axs:
    ax.contour(thickness, intensity, prob, cmap=cmaps[0].reversed(), levels=levels, alpha=.5, antialiased=True, zorder=-1)

interv_i = [96, 160, 224]
interv_t = [1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5]

# first intervene on i
for do_i in interv_i:
    cond_i = int_cond_dict['do_i'][do_i]
    thickness = cond_i['thickness']
    prob = cond_i['prob']
    color = cmaps[1](.75)
    
    my_violinplot(thickness[:, 0], prob, do_i, vert=False, ax=axs[0], width=.75*50,
                  fill_kw=dict(alpha=.7, facecolor=color), line_kw=dict(color=color, ls=':'))

# condition on t
for do_t in interv_t:
    cond_t = int_cond_dict['cond_t'][do_t]
    intensity = cond_t['intensity']
    prob = cond_t['prob']
    color = cmaps[2](.75)
    my_violinplot(intensity[0, :], prob, do_t, vert=True, ax=axs[1], width=.75*1.,
                  fill_kw=dict(alpha=.7, facecolor=color), line_kw=dict(color=color, ls=':'))

ind_cond_dict = torch.load("morphomnist/generated_data_conditional/version_5/int_cond_dict.pt")
# condition on t
for do_t in interv_t:
    cond_t = ind_cond_dict['cond_t'][do_t]
    intensity = cond_t['intensity']
    prob = cond_t['prob_2']
    color = cmaps[2](.75)
    my_violinplot(intensity[0, :], prob, do_t, vert=True, ax=axs[2], width=.75*1.,
                  fill_kw=dict(alpha=.7, facecolor=color), line_kw=dict(color=color, ls=':'))


axs[0].set_title("$p(t\,|\,do(i)) = p(t\,|\,i)$")

for ax in axs:
    ax.set_xlabel('Thickness ($t$)', fontsize='medium')
axs[0].set_ylabel('Intensity ($i$)', fontsize='medium');

axs[1].set_title("$p(i\,|\,t)$")
axs[2].set_title("$p(i\,|\,do(t)) = p(i)$")
fig.savefig(f'./thesis_plots/int_cond_full.pdf', bbox_inches='tight',dpi=100)
plt.show()


# %% Counterfactuals

paths = [f'{x}counterfactuals.pt' for x in source[1:]]
titles = ['Ind.','Cond.', 'Rev.', 'Full']
test_data = pd.read_csv('generated_data/t10k-morpho.csv',index_col=0)


import matplotlib as mpl
mpl.rcParams['figure.dpi'] = 300

from matplotlib import cm
cmaps = [cm.Reds, cm.Blues, cm.Greens]
img_cm = 'Greys_r'

# start with conditional
num = 3
path = paths[num]
title = titles[num]

cf = torch.load(path)
all_thickness, all_intensity, all_images, interventions = cf.values()

num_idx = 3
num_inter = 6
hrs = [0.2 if ((i + 1) % 3) == 0 else 1 for i in range(2 * num_idx + num_idx - 1)]
fig, ax = plt.subplots(2 * num_idx + num_idx - 1, (num_inter + 1), figsize=(1.8 * (num_inter + 1), 4*num_idx),
                           gridspec_kw=dict(wspace=0, hspace=0, height_ratios=hrs))


# we will want to loop over this over k = 0,1,2

for k in range(3):
    lim = 0

    ims = all_images[(k*7):(k*7+7)]
    orig = ims[0]
    counterfactuals = ims[1:]
    t = all_thickness[k]
    i = all_intensity[k]

    for idx, counterfactual in enumerate(counterfactuals):

        diff = (orig - counterfactual).squeeze().cpu()
        lim = np.maximum(lim, diff.abs().max())

    ax[k * 3, 0].imshow(orig.cpu().squeeze(), 'Greys_r', vmin=0, vmax=255)
    ax[k * 3, 0].set_title('Original')

    for idx, intervention in enumerate(interventions):
        x = counterfactuals[idx]

        diff = (orig - x).squeeze()

        feature, value = list(intervention.items())[0][0], list(intervention.items())[0][1]
        ax[k * 3, idx + 1].set_title(f'do({feature[0]}={value:.0f})')
        ax[k * 3, idx + 1].imshow(x.cpu().numpy().squeeze(), 'Greys_r', vmin=0, vmax=255)
        ax[k * 3 + 1, idx + 1].imshow(diff.cpu().numpy(), 'seismic', clim=[-lim, lim])

    att_str = f"$t={t.item():.1f}$\n$i={i.item():.0f}$"
    ax[k * 3 + 1, 0].text(0.5, 0.5, att_str, horizontalalignment='center',
                                    verticalalignment='center', transform=ax[k * 3 + 1, 0].transAxes,
                                    fontsize=mpl.rcParams['axes.titlesize'])

    for axi in ax.ravel():
            axi.axis('off')
            axi.xaxis.set_major_locator(plt.NullLocator())
            axi.yaxis.set_major_locator(plt.NullLocator())


fig.savefig(f'./thesis_plots/counterfactuals_{title}.pdf', bbox_inches='tight',dpi=100)
plt.show()

# # %% Counterfactual traversal? (wait with this..)


# %% Additional plots of intervention on data

import torch
import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pyro
from pyro.distributions import Gamma, Normal, TransformedDistribution, Categorical, Uniform
from pyro.distributions.transforms import SigmoidTransform, AffineTransform, ComposeTransform


n_sam = 500

sns.set_theme()

def model_o(n_samples=None, scale=0.5, invert=False):
    with pyro.plate('observations', n_samples):
        eps_i1 = pyro.sample("eps_i1", Gamma(10,5)) + 0.5
        eps_i2 = pyro.sample("eps_i2", Normal(0,1))
        
        intensity = 190 * SigmoidTransform()(0.5 * eps_i2 + 2 * eps_i1 - 5) + 64
        
        eps_t = pyro.sample("eps_t", Normal(0,1))
        thickness =  torch.max(-torch.log(-(intensity-255)/(intensity-64)) + 4 - 1/2 * eps_t,torch.tensor([0.3]))
        
    return thickness, intensity

t, i = model_o(n_sam)
t1, i1 = t +1, i
t2, i2 = t-1/2, i

# % plots
fig = plt.figure(figsize=(15, 5))
ratio = 5
global_gs = fig.add_gridspec(1, 2)
    
ax_joint0 = ax_marg_x0 = ax_marg_y0 = None

gs = global_gs[0,0].subgridspec(2, 2, width_ratios=[ratio, 1], height_ratios=[1, ratio],
                                wspace=0.1, hspace=0.1)


ax_joint = fig.add_subplot(gs[1, 0], sharex=ax_joint0, sharey=ax_joint0)
ax_marg_x = fig.add_subplot(gs[0, 0], sharex=ax_joint0, sharey=ax_marg_x0)
ax_marg_y = fig.add_subplot(gs[1, 1], sharey=ax_joint0, sharex=ax_marg_y0)
# if i % 2 == 0:
#     ax_joint0 = ax_joint
#     ax_marg_x0 = ax_marg_x
#     ax_marg_y0 = ax_marg_y

plt.setp(ax_marg_x.get_xticklabels(), visible=False)
plt.setp(ax_marg_y.get_yticklabels(), visible=False)

# Turn off the ticks on the density axis for the marginal plots
plt.setp(ax_marg_x.yaxis.get_majorticklines(), visible=False)
plt.setp(ax_marg_x.yaxis.get_minorticklines(), visible=False)
plt.setp(ax_marg_y.xaxis.get_majorticklines(), visible=False)
plt.setp(ax_marg_y.xaxis.get_minorticklines(), visible=False)
plt.setp(ax_marg_x.get_yticklabels(), visible=False)
plt.setp(ax_marg_y.get_xticklabels(), visible=False)
for s in ['top', 'right']:
    ax_joint.spines[s].set_visible(False)
for s in ['left', 'top', 'right']:
    ax_marg_x.spines[s].set_visible(False)
for s in ['top', 'right', 'bottom']:
    ax_marg_y.spines[s].set_visible(False)
ax_marg_x.yaxis.grid(False)
ax_marg_y.xaxis.grid(False)

#sns.kdeplot(x = t, y=i, ax=ax_joint, cmap=sns.color_palette("light:red", as_cmap=True), legend='$p(t,i)$', alpha=0.7, levels = 20)
sns.kdeplot(x = t, y=i, ax=ax_joint, cmap=sns.color_palette("blend:red,salmon", as_cmap=True), alpha = 0.4,levels=7,fill=True)
sns.kdeplot(x = t1, y=i1, ax=ax_joint, cmap=sns.color_palette("blend:blue,skyblue", as_cmap=True), alpha=0.6,levels=7)
sns.kdeplot(x = t2, y=i2, ax=ax_joint, cmap=sns.color_palette("blend:green,palegreen", as_cmap=True),levels=5, linestyles='dashed')

sns.kdeplot(x = t, ax=ax_marg_x, color='salmon', alpha = 0.7, fill=True)
sns.kdeplot(x = t1, ax=ax_marg_x, color='skyblue')
sns.kdeplot(x = t2, ax=ax_marg_x, color='palegreen', linestyle='dashed')

sns.kdeplot(y = i, ax=ax_marg_y, color='salmon', alpha = 0.7, fill=True)
sns.kdeplot(y = i1, ax=ax_marg_y, color='skyblue')
sns.kdeplot(y = i2, ax=ax_marg_y, color='palegreen',linestyle='dashed')

ax_joint.set_xlabel('Thickness')
ax_joint.set_ylabel('Intensity')
ax_joint.set_xlim(0,10)
ax_joint.set_ylim(64,255)

ax_joint.plot(0,0,color='salmon', label='$p(t,i)$')
ax_joint.plot(0,0,color='skyblue', label='$p(t,i|do(t+1))$')
ax_joint.plot(0,0,color='palegreen', label='$p(t,i|do(t-1/2))$',linestyle='dashed')

ax_joint.legend(loc='lower right')

plt.savefig('thesis_plots/do_t.pdf')

# % Second one for intensity


def model_o(n_samples=None, scale=0.5, invert=False):
    with pyro.plate('observations', n_samples):
        eps_i1 = pyro.sample("eps_i1", Gamma(10,5)) + 0.5
        eps_i2 = pyro.sample("eps_i2", Normal(0,1))
        
        intensity = 190 * SigmoidTransform()(0.5 * eps_i2 + 2 * eps_i1 - 5) + 64
        
        eps_t = pyro.sample("eps_t", Normal(0,1))
        thickness =  torch.max(-torch.log(-(intensity-255)/(intensity-64)) + 4 - 1/2 * eps_t,torch.tensor([0.3]))
        
    return thickness, intensity

def model_1(n_samples=None, scale=0.5, invert=False):
    with pyro.plate('observations', n_samples):
        eps_i1 = pyro.sample("eps_i1", Gamma(10,5)) + 0.5
        eps_i2 = pyro.sample("eps_i2", Normal(0,1))
        
        intensity = 190 * SigmoidTransform()(0.5 * eps_i2 + 2 * eps_i1 - 5) + 64 + 32
        
        eps_t = pyro.sample("eps_t", Normal(0,1))
        thickness =  torch.max(-torch.log(-(intensity-255)/(intensity-64)) + 4 - 1/2 * eps_t,torch.tensor([0.3]))
        
    return thickness, intensity


def model_2(n_samples=None, scale=0.5, invert=False):
    with pyro.plate('observations', n_samples):
        eps_i1 = pyro.sample("eps_i1", Gamma(10,5)) + 0.5
        eps_i2 = pyro.sample("eps_i2", Normal(0,1))
        
        intensity = 190 * SigmoidTransform()(0.5 * eps_i2 + 2 * eps_i1 - 5) + 64 - 32
        
        eps_t = pyro.sample("eps_t", Normal(0,1))
        thickness =  torch.max(-torch.log(-(intensity-255)/(intensity-64)) + 4 - 1/2 * eps_t,torch.tensor([0.3]))
        
    return thickness, intensity
# 


t, i = model_o(n_sam)
t1, i1 = model_1(n_sam)
t2, i2 = model_2(n_sam)

#fig = plt.figure(figsize=(15, 5))
#ratio = 5

gs = global_gs[0,1].subgridspec(2, 2, width_ratios=[ratio, 1], height_ratios=[1, ratio],
                                wspace=0.1, hspace=0.1)


ax_joint = fig.add_subplot(gs[1, 0], sharex=ax_joint0, sharey=ax_joint0)
ax_marg_x = fig.add_subplot(gs[0, 0], sharex=ax_joint0, sharey=ax_marg_x0)
ax_marg_y = fig.add_subplot(gs[1, 1], sharey=ax_joint0, sharex=ax_marg_y0)
# if i % 2 == 0:
#     ax_joint0 = ax_joint
#     ax_marg_x0 = ax_marg_x
#     ax_marg_y0 = ax_marg_y

plt.setp(ax_marg_x.get_xticklabels(), visible=False)
plt.setp(ax_marg_y.get_yticklabels(), visible=False)

# Turn off the ticks on the density axis for the marginal plots
plt.setp(ax_marg_x.yaxis.get_majorticklines(), visible=False)
plt.setp(ax_marg_x.yaxis.get_minorticklines(), visible=False)
plt.setp(ax_marg_y.xaxis.get_majorticklines(), visible=False)
plt.setp(ax_marg_y.xaxis.get_minorticklines(), visible=False)
plt.setp(ax_marg_x.get_yticklabels(), visible=False)
plt.setp(ax_marg_y.get_xticklabels(), visible=False)
for s in ['top', 'right']:
    ax_joint.spines[s].set_visible(False)
for s in ['left', 'top', 'right']:
    ax_marg_x.spines[s].set_visible(False)
for s in ['top', 'right', 'bottom']:
    ax_marg_y.spines[s].set_visible(False)
ax_marg_x.yaxis.grid(False)
ax_marg_y.xaxis.grid(False)

sns.kdeplot(x = t, y=i, ax=ax_joint, cmap=sns.color_palette("blend:red,salmon", as_cmap=True), alpha = 0.4,levels=7,fill=True)
sns.kdeplot(x = t1, y=i1, ax=ax_joint, cmap=sns.color_palette("blend:blue,skyblue", as_cmap=True), alpha=0.6,levels=7)
sns.kdeplot(x = t2, y=i2, ax=ax_joint, cmap=sns.color_palette("blend:green,palegreen", as_cmap=True),levels=5, linestyles='dashed')

sns.kdeplot(x = t, ax=ax_marg_x, color='salmon', alpha = 0.7, fill=True)
sns.kdeplot(x = t1, ax=ax_marg_x, color='skyblue')
sns.kdeplot(x = t2, ax=ax_marg_x, color='palegreen', linestyle='dashed')

sns.kdeplot(y = i, ax=ax_marg_y, color='salmon', alpha = 0.7, fill=True)
sns.kdeplot(y = i1, ax=ax_marg_y, color='skyblue')
sns.kdeplot(y = i2, ax=ax_marg_y, color='palegreen',linestyle='dashed')

ax_joint.set_xlabel('Thickness')
ax_joint.set_ylabel('Intensity')

ax_joint.plot(0,0,color='salmon', label='$p(t,i)$')
ax_joint.plot(0,0,color='skyblue', label='$p(t,i|do(i+32))$')

ax_joint.plot(0,0,color='palegreen', label='$p(t,i|do(i-32))$',linestyle='dashed')
ax_joint.set_xlim(0,10)
ax_joint.set_ylim(64,255)

ax_joint.legend(loc='lower right')

fig.tight_layout()
fig.savefig(f'./thesis_plots/test_interventions.pdf', bbox_inches='tight',dpi=100)


# %% Intervention figures over intensity

paths = [f'{x}prob_maps_int.pt' for x in source[2:]]
titles = ['Cond.', 'Rev.', 'Full']

import matplotlib as mpl
mpl.rcParams['figure.dpi'] = 300

from matplotlib import cm
cmaps = [cm.Reds, cm.Blues, cm.Greens]
img_cm = 'Greys_r'


prob_maps = {}

for path, title in zip(paths, titles):
    prob_maps[title] = torch.load(path)


intensity_range = torch.arange(64, 255, 0.5, dtype=torch.float)
thickness_range = torch.arange(.5, 9, 0.01, dtype=torch.float)


num_intensity = intensity_range.shape[0]
num_thickness = thickness_range.shape[0]

intensity_range = intensity_range.repeat(num_thickness).unsqueeze(1)
thickness_range = thickness_range.repeat_interleave(num_intensity).unsqueeze(1)


from matplotlib.colors import ListedColormap

def make_transparent_cmap(cmap):
    n = 256
    colours = cmap(np.linspace(0, 1, n))
    colours[:, -1] = np.linspace(0, 1, n)
    return ListedColormap(colours) 




# %% plottings
def plot_exp(i, exp, ax_joint, ax_marg_x, ax_marg_y):
    contours = []
    for j, (k, v) in enumerate(prob_maps[exp].items()):
        thickness = v['thickness'].reshape(num_thickness, num_intensity).numpy()
        intensity = v['intensity'].reshape(num_thickness, num_intensity).numpy()
        log_prob = v['log_prob'].reshape(num_thickness, num_intensity).numpy()
        prob = np.exp(log_prob)


        valid = thickness[:, 0] > 0.5
        thickness = thickness[valid, :]
        intensity = intensity[valid, :]
        log_prob = log_prob[valid, :]
        prob = prob[valid, :]
        
        dt = thickness[1, 0] - thickness[0, 0]
        di = intensity[0, 1] - intensity[0, 0]
        marg_t = prob.sum(1) * dt
        marg_i = prob.sum(0) * di
        
        mi = prob.min()
        ma = np.percentile(prob.flat, 99.5)
        step = (ma - mi) / 5
        levels = np.arange(mi, ma, step) + step
        
        filled = j == 0
        cmap = cmaps[j]
        colour = cmap(.7)
        
        alpha = .7
        if filled:
            ax_joint.contourf(thickness, intensity, prob, cmap=cmap, antialiased=True, levels=levels, alpha=alpha, extend='max')
            c = ax_marg_x.fill_between(thickness[:, 0], marg_t, color=colour, lw=0, alpha=alpha)
            ax_marg_y.fill_betweenx(intensity[0, :], marg_i, color=colour, lw=0, alpha=alpha)
        else:
            kwargs = dict(cmap=cmap.reversed(), antialiased=True, levels=levels, alpha=alpha)
            if j == 2:
                kwargs['linestyles'] = 'dashed'
            c = ax_joint.contour(thickness, intensity, prob, **kwargs)
        
        kwargs = dict(ls='--') if j == 2 else {}
        h, = ax_marg_x.plot(thickness[:, 0], marg_t, c=colour, **kwargs)
        ax_marg_y.plot(marg_i, intensity[0, :], c=colour, **kwargs)
        
        if filled:
            contours.append(c)
        else:
            contours.append(h)            

    ax_marg_x.set_title(exp)
    ax_joint.set_xlabel('thickness ($t$)', fontsize='medium')
    if i == 0:
        ax_joint.set_ylabel('intensity ($i$)', fontsize='medium')
        ax_joint.legend(contours, prob_maps[exp].keys(), loc='lower right', fontsize='small')

titles = ['Cond.', 'Rev.', 'Full']

def plot_all_joints():
    n_exp = len(titles)
    fig = plt.figure(figsize=(15, 5))
    ratio = 5
    global_gs = fig.add_gridspec(1, 3)
    
    ax_joint0 = ax_marg_x0 = ax_marg_y0 = None

    margin = 0.02 / n_exp
    width = 1. / n_exp
    for i, exp in enumerate(titles):
        gs = global_gs[0,i].subgridspec(2, 2, width_ratios=[ratio, 1], height_ratios=[1, ratio],
                                wspace=0.1, hspace=0.1)

        ax_joint = fig.add_subplot(gs[1, 0], sharex=ax_joint0, sharey=ax_joint0)
        ax_marg_x = fig.add_subplot(gs[0, 0], sharex=ax_joint0, sharey=ax_marg_x0)
        ax_marg_y = fig.add_subplot(gs[1, 1], sharey=ax_joint0, sharex=ax_marg_y0)

        ax_joint.set_xlim(0,10)
        ax_joint.set_ylim(64,255)
        # if i % 2 == 0:
        #     ax_joint0 = ax_joint
        #     ax_marg_x0 = ax_marg_x
        #     ax_marg_y0 = ax_marg_y
        
        plt.setp(ax_marg_x.get_xticklabels(), visible=False)
        plt.setp(ax_marg_y.get_yticklabels(), visible=False)
        
        # Turn off the ticks on the density axis for the marginal plots
        plt.setp(ax_marg_x.yaxis.get_majorticklines(), visible=False)
        plt.setp(ax_marg_x.yaxis.get_minorticklines(), visible=False)
        plt.setp(ax_marg_y.xaxis.get_majorticklines(), visible=False)
        plt.setp(ax_marg_y.xaxis.get_minorticklines(), visible=False)
        plt.setp(ax_marg_x.get_yticklabels(), visible=False)
        plt.setp(ax_marg_y.get_xticklabels(), visible=False)
        for s in ['top', 'right']:
            ax_joint.spines[s].set_visible(False)
        for s in ['left', 'top', 'right']:
            ax_marg_x.spines[s].set_visible(False)
        for s in ['top', 'right', 'bottom']:
            ax_marg_y.spines[s].set_visible(False)
        ax_marg_x.yaxis.grid(False)
        ax_marg_y.xaxis.grid(False)

        if exp == 'True':
            #ax_joint.hist2d(t_s.cpu().numpy(),i_s.cpu().numpy(), bins=100, density=True, cmin=0.001)
            #sns.kdeplot()
            pass
        else:
            # if i % 2 == 0:
            #     plt.setp(ax_joint.get_yticklabels(), visible=False)

            plot_exp(i, exp, ax_joint, ax_marg_x, ax_marg_y)
    
    fig.tight_layout()
    fig.savefig(f'./thesis_plots/intervention_intensity_kde.pdf', bbox_inches='tight',dpi=100)
plot_all_joints()
plt.show()
# %%
