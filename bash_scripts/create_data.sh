#!/bin/sh 
### General options 
### -- specify queue -- 
#BSUB -q hpc
### -- set the job Name -- 
#BSUB -J My_Application
### -- ask for number of cores (default: 1) -- 
#BSUB -n 24
### -- specify that the cores must be on the same host -- 
#BSUB -R "span[hosts=1]"
### -- specify that we need 4GB of memory per core/slot -- 
#BSUB -R "rusage[mem=10GB]"
### -- specify that we want the job to get killed if it exceeds 3 GB per core/slot -- 
#BSUB -M 12GB
### -- set walltime limit: hh:mm -- 
#BSUB -W 24:00 
### -- set the email address -- 
# please uncomment the following line and put in your e-mail address,
# if you want to receive e-mail notifications on a non-default address
##BSUB -u your_email_address
### -- send notification at start -- 
#BSUB -B 
### -- send notification at completion -- 
#BSUB -N 
### -- Specify the output and error file. %J is the job-id -- 
### -- -o and -e mean append, -oo and -eo mean overwrite -- 
#BSUB -o out/Output_%J.out 
#BSUB -e err/Output_%J.err 

# here follow the commands you want to execute with input.in as the input file
# obtain job num
job_num=$LSB_JOBID

# activate anaconda without argument error
function DoSource(){
    source "/work3/s164180/miniconda3/bin/activate";
    conda activate thesis;
}
DoSource

python -m deepscm.datasets.morphomnist.create_synth_data --model model_test
python -m deepscm.datasets.morphomnist.create_synth_data --model model_thickness_intensity
#python -m deepscm.datasets.morphomnist.create_synth_data --model model_thickness_slant
#python -m deepscm.datasets.morphomnist.create_synth_data --model model_thickness_width