#!/bin/sh
### General options
### –- specify queue --
#BSUB -q gpua10

### -- set the job Name --
#BSUB -J jupyter
### -- ask for number of cores (default: 1) --
#BSUB -n 16
### -- Select the resources: 1 gpu in exclusive process mode --
#BSUB -gpu "num=1:mode=exclusive_process"
###BSUB -R "select[gpu24gb]", we uncomment this so we simple use the first available GPU
### -- set walltime limit: hh:mm --  maximum 24 hours for GPU-queues right now
#BSUB -W 24:00
# request 5GB of system-memory
#BSUB -R "rusage[mem=4GB]"
### -- set the email address --
# please uncomment the following line and put in your e-mail address,
# if you want to receive e-mail notifications on a non-default address
##BSUB -u your_email_address
### -- send notification at start --
#BSUB -B
### -- send notification at completion--
#BSUB -N
### -- Specify the output and error file. %J is the job-id --
### -- -o and -e mean append, -oo and -eo mean overwrite --
#BSUB -o out/%J.out
#BSUB -e err/%J.err
# -- end of LSF options --


# activate anaconda without argument error
function DoSource(){
    source "/work3/s164180/miniconda3/bin/activate";
    conda activate thesis;
}
DoSource

#jupyter notebook --no-browser --port=40000 --ip=$HOSTNAME
jupyter notebook --no-browser --port=4242 --ip=$HOSTNAME

#ssh -L 40000:local_host:8000 s164180@login2.gbar.dtu.dk:$HOSTNAME